<?php include('header.php');
include('connect.php');
include('db-produto.php');

$nome = $_GET['nome'];

if(removeProduto($conexao, $nome)) {
    ?>
    <p><?= $nome; ?>, removido com sucesso!</p>
    <?php
} else {
    ?>
    <p><?= $nome; ?>, não pode ser removido!</p>
    <?= mysqli_error($conexao); ?>
    <?php    
}
?>