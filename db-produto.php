<?php

function buscaProduto($conexao, $nome) {
    $query = "SELECT * FROM produto WHERE nome = '{$nome}'";
    $produto = mysqli_query($conexao, $query);
    return mysqli_fetch_assoc($produto);
}

function listaProdutos($conexao) {
    $produtos = [];
    $resultado = mysqli_query($conexao, "select * from produto");

    while($produto = mysqli_fetch_assoc($resultado)) {
        array_push($produtos, $produto);
    }

    return $produtos;
}

function insereProduto($conexao, $nome, $autor, $qtd_paginas, $preco, $dt_inclusao) {
    $query = "INSERT INTO `produto` (`nome`, `autor`, `qtd_paginas`, `preco`, `ativo`, `dt_inclusao`) VALUES ('{$nome}', '{$autor}', '{$qtd_paginas}', '{$preco}', 'Inativo', '{$dt_inclusao}')";
    return mysqli_query($conexao, $query);
}

function removeProduto($conexao, $nome) {
    $query = "DELETE FROM `produto` WHERE `produto`.`nome` = '{$nome}'";
    return mysqli_query($conexao, $query);
}

function alteraProduto($conexao, $nome, $autor, $qtd_paginas, $preco, $ativo, $dt_inclusao) {
    $query = "UPDATE produto SET 
        nome = '{$nome}', 
        autor = '{$autor}', 
        preco = '{$preco}', 
        qtd_paginas = '{$qtd_paginas}', 
        ativo = '{$ativo}', 
        dt_inclusao = '{$dt_inclusao}' 
    WHERE produto.nome = '{$nome}'";
    return mysqli_query($conexao, $query);
}