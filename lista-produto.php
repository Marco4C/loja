<?php include('header.php')?>
<?php include('connect.php')?>
<?php include('db-produto.php')?>

<ol>
<?php
$produtos = listaProdutos($conexao);
foreach($produtos as $produto) {
    echo "
        <li>
            <ul>
                <li>Título: " . $produto['nome'] . "</li>
                <li>Autor: " . $produto['autor'] . "</li>
                <li>Preço: R$ " . number_format($produto['preco'], 2, ',', '.') . "</li>
                <li>Páginas: " . $produto['qtd_paginas'] . "</li>
                <li>Ação: " . $produto['ativo'] . "</li>
                <li>Inclusão: " . $produto['dt_inclusao'] . "</li>
                <a href='formulario-altera-produto.php?nome={$produto['nome']}'>alterar</a><br/>
                <a href='remove-produto.php?nome={$produto['nome']}'>remover</a>
            </ul>
        </li></br>
    ";
}
?>
</ol>

<?php include('footer.php')?>