<?php include('header.php')?>
<?php include('connect.php')?>
<?php include('db-produto.php')?>

<?php
$nome = $_GET['nome'];
$produto = buscaProduto($conexao, $nome);
$ativo = $produto['ativo'] == "Ativo";
?>

<form action="altera-produto.php">
    Nome: <input type="text" name="nome" value="<?= $produto['nome']?>"/><br/>
    Autor: <input type="text" name="autor" value="<?= $produto['autor']?>"/><br/>
    Qtd. de páginas: <input type="number" min="1" name="qtd_paginas" value="<?= $produto['qtd_paginas']?>"/><br/>
    Preço: <input type="number" min="0.01" step="0.01" name="preco" value="<?= $produto['preco']?>"/><br/>
    
    
    Status:
    <?php
    if($ativo){
    ?>
        <input type="radio" name="ativo" checked="checked" value="ativo">Ativo
        <input type="radio" name="ativo" value="ativo">Inativo</br>
    <?php
    } else {
    ?>
        <input type="radio" name="ativo" value="Ativo">Ativo
        <input type="radio" name="ativo" checked="checked" value="Inativo">Inativo</br>
    <?php
    }
    ?>


    Dt. de inclusão: <input type="text" name="dt_inclusao" value="<?= $produto['dt_inclusao']?>"/><br/>
    <input type="submit" value="Alterar" />
</form>

<?php include('footer.php')?>