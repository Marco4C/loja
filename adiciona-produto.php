<?php include('header.php')?>
<?php include('connect.php')?>
<?php include('db-produto.php')?>

<?php 

$nome = $_GET['nome'];
$autor = $_GET['autor'];
$qtd_paginas = $_GET['qtd_paginas'];
$preco = $_GET['preco'];
$dt_inclusao = $_GET['dt_inclusao'];

//Verifica se nome do livro e do autor têm 
//três ou mais caracteres e quantidade de páginas e preços válidos
if (strlen($nome) > 3 && strlen($autor) > 3 && $qtd_paginas >= 1 && $preco >= 0.01)
{
    if(insereProduto($conexao, $nome, $autor, $qtd_paginas, $preco, $dt_inclusao)) {
        ?>
        <p><?= $nome; ?>, adicionado com sucesso!</p>
        <?php
    } else {
        ?>
        <p><?= $nome; ?>, não pode ser adicionado!</p>
        <?php    
    }
} else {
    ?>
    <p><?= $nome; ?>, não pode ser adicionado!</p>
    <?php
}
?>

<?php include('footer.php')?>